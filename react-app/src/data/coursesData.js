const coursesData = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore modi animi expedita eligendi eaque sint at vitae. Consequuntur, itaque impedit sunt doloremque sit reprehenderit dolorem adipisci  vero aliquid, velit est.",
    price: 45000,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python-Django",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore modi animi expedita eligendi eaque sint at vitae. Consequuntur, itaque impedit sunt doloremque sit reprehenderit dolorem adipisci  vero aliquid, velit est.",
    price: 50000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java-Springot",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore modi animi expedita eligendi eaque sint at vitae. Consequuntur, itaque impedit sunt doloremque sit reprehenderit dolorem adipisci  vero aliquid, velit est.",
    price: 55000,
    onOffer: true,
  },
];

export default coursesData; // Export the coursesData array to use in index.js
