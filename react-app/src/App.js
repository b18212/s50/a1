//! This is root file

import { useEffect, useState } from "react";
import { Container } from "react-bootstrap"; // Import from react bootstrap components
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
// import Banner from "./components/Banner";
// import Highlights from "./components/Highlights";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Register from "./pages/Register";
import Login from "./pages/Login";
import "./App.css";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import { UserProvider } from "./UserContext";

// log in and log out
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  //log out
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    // console.log(user);
    // console.log(localStorage);
    //*from userRoutes/js under get user details
    // once log in it will not log out when you refresh the page
    fetch("http://localhost:4000/users/getUserDetails", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        //* capture the data of whoever log in
        // setting it properly
        // set the user states values with the user details upon sucessful login
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          // if not log in it should be null
          //set back the intial of user
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    // return a container div
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route
              exact
              path="/courseView/:courseId"
              element={<CourseView />}
            />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App; // Export the App component to use in index.js
