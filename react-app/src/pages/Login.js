import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2"; // no need to put curly braces
import UserContext from "../UserContext";

export default function Login(props) {
  console.log(props);

  //setUser from provider
  const { user, setUser } = useContext(UserContext);
  console.log(user);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    //*fetch connected to the backend route from userRoutes.js login to make a connection
    //* also connected to userControllers.js
    fetch("http://localhost:4000/users/login", {
      //needed options to make a connection
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email, //value needed to login
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // data captured from the backend
        //end of fetch

        //* manipulate data
        //* access token will be generated and stored in the local storage
        //* goal is to set the item or token to local storage
        //* token should be defined as a string
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken); //invoke the function to retrieve the user details

          //* Swal alert to show that the user has successfully logged in
          //* also based on userControllers.js at User.findOne
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Booking App of 182!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your credentials",
          });
        }
      });

    // localStorage.setItem('email', email)

    // setUser({
    //     email: localStorage.getItem('email')
    // });

    // Clear input fields after submission
    // setter works asynchronously
    setEmail("");
    setPassword("");

    // alert(`${email} has been verified! Welcome back!`);
  }

  //* extracting the token from the local storage
  //* to get the decoded token from userConrollers.js
  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/getUserDetails", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        // we just following the standard of JWT
        // "Bearer" is the type of token we are using in this case JWT
        // no need to put Method as this is retrieved from the local storage already
      },
    })
      .then((res) => res.json()) //communication from the database front end to the backend
      .then((data) => {
        console.log(data);

        //once it was properly read it will retrieve the data from the fetch

        // for verification of the user if the user is logged in
        setUser({
          _id: data._id, //to tell the user who's log in
          isAdmin: data.isAdmin, //to tell what features to show
        });
      });
  };

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <>
      <h1>Login Here:</h1>
      <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        <p>
          Not yet registered <Link to="/register">Register Here</Link>
        </p>

        {isActive ? (
          <Button
            variant="success"
            type="submit"
            id="submitBtn"
            className="mt-3 mb-5"
          >
            Submit
          </Button>
        ) : (
          <Button
            variant="danger"
            type="submit"
            id="submitBtn"
            className="mt-3 mb-5"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
