// import coursesData from '../data/coursesData';
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {
  // console.log(coursesData);
  // holds the mock database
  // console.log(coursesData[0]);
  // captured the first object in coursesData

  // const courses = coursesData.map(course => {
  // 	console.log(course.id)
  // 	return(
  // 		<CourseCard key = {course.id} courseProp = {course}/>
  // 	)
  // })

  const [courses, setCourses] = useState([]);

  //
  useEffect(() => {
    fetch("http://localhost:4000/courses")
      .then((res) => res.json())
      .then((data) => {
        console.log(data); // to check if the data is being fetched
        //this will retreive the the courses from the database

        //.map is a looping function that will loop through the data and create a new array of course cards
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1> Courses Available: </h1>
      {courses}
    </>
  );
}
