// import { useState, useEffect } from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  console.log(courseProp);
  // console.log(props); // Prints the props object
  // result: coursesData [0] - wdc001
  // console.log(typeof props);
  // result: object

  //* object destructuring
  //* getting info from coursesData.js
  //* we can use _id to use this as a path going to course
  const { name, description, price, _id } = courseProp;
  // console.log(name);

  //* getter
  // is the first one handles the initial state
  //* setter
  // can make changes from the getter
  //*Syntax:
  //const [getter, setter] = useState(initialValueOfGetter);
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // const [isOpen, setIsOpen] = useState(false);

  //*useState returns an array with two elements
  //* this was commented out because the enroll will be fetch on the enroll function
  // const enroll = () => {
  //   setCount(count + 1); // setCount is the setter if you use count++ you will get an error because you are trying to change the state of the count while count + 1 is the getter of the count state (count + 1 is the new state) and you are trying to change the state of the count to the new state (count + 1).
  //   console.log(`Enrollees ${count}`);

  //   if (seats > 0) {
  //     setCount(count + 1);
  //     console.log("Enrollees: " + count);
  //     setSeats(seats - 1);
  //     console.log("Seats: " + seats);
  //   } else {
  //     alert("No more seats available");
  //   }
  // };

  //useEffect is a hook that runs after the component is rendered
  // useEffect(() => {
  //   if (seats === 0) {
  //     setIsOpen(true);
  //   }
  // }, [seats]);

  //* it was also removed the seats and enrollment because it was result undefined
  return (
    <Card className="mb-4">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Link className="btn btn-primary" to={`/courseView/${_id}`}>
          View Details
        </Link>
      </Card.Body>
    </Card>
  );
}

// This was removed <Link variant="primary" onClick={enroll} disabled={isOpen}>Enroll</Link>

/*
let arr1 = [1, 2, 3]

Array Destructuring
const [one, two, three] = arr1;

*/
