import { Row, Col, Card } from "react-bootstrap";
export default function Higlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Learn From Home</h2>
            </Card.Title>

            <Card.Text>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
              modi animi expedita eligendi eaque sint at vitae. Consequuntur,
              itaque impedit sunt doloremque sit reprehenderit dolorem adipisci
              vero aliquid, velit est.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Study Now, Pay Later</h2>
            </Card.Title>

            <Card.Text>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
              modi animi expedita eligendi eaque sint at vitae. Consequuntur,
              itaque impedit sunt doloremque sit reprehenderit dolorem adipisci
              vero aliquid, velit est.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Be Part Of Our Community</h2>
            </Card.Title>

            <Card.Text>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
              modi animi expedita eligendi eaque sint at vitae. Consequuntur,
              itaque impedit sunt doloremque sit reprehenderit dolorem adipisci
              vero aliquid, velit est.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
