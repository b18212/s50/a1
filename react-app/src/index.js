import React from "react"; // Import from react
import ReactDOM from "react-dom/client"; // Import from react-dom
import App from "./App"; // Import from App.js // App is a component
import "bootstrap/dist/css/bootstrap.min.css"; // Import bootstrap CSS to use the bootstrap classes

// Render the App component to the DOM
const root = ReactDOM.createRoot(document.getElementById("root"));
//you will use # sign if your are using (document.querySelector("#root"))
// main appplication to be rendered
// creating a main entry point
// Create a root element to render the App component to the DOM
root.render(
  //
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// tries to find element with an id root to index.html. You need to specify wha t do you want to display
// index.js was first one importing in the app file

// const name = "Tony";

// const user = {
//     firstName: "Peter",
//     lastName: "Parker",
// };

// const formatName = (user) => {
//     return `${user.firstName} ${user.lastName}`;
// };

// const element = <h1> Hello,
//     {formatName(user)} </h1>;

// //targeting the root file from index.html
// const root = ReactDOM.createRoot(document.getElementById("root")); // create a root element

// root.render(element); // renders the element to the DOM

// ReactDOM.render(element, document.getElementById("root")); // renders the element to the DOM
